<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoInBundling extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'video_in_bundling';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */


    //public $timestamps = false;
}
