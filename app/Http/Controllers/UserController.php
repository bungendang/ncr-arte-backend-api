<?php

namespace App\Http\Controllers;

use App\User;
use App\UserInfo;
use App\VideoPurchase;
use Illuminate\Http\Request;
use Illuminate\Hashing\BcryptHasher;
use Tymon\JWTAuth\JWTAuth;
use DB;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $jwt;

    public function __construct(JWTAuth $jwt)
    {
        $this->jwt = $jwt;
    }

    public function postRegister(Request $request)
    {  
        if (User::where('username',$request->username)->first()) {
            # code...
            return response()->json(['status'=>'error','message'=>'Username / Email already exist']);
        } else {
            # code...
            $user = new User;
            $user->username = $request->username;
            $user->email = $request->username;
            $user->password = (new BcryptHasher)->make($request->password);
            $user->role_id = 1;
            $user->save();

            $id = $user->id;

            //return $id;

            $userinfo = new UserInfo;
            $userinfo->userId = $id;
            $userinfo->firstname = $request->firstname;
            $userinfo->lastname = $request->lastname;
            $userinfo->address = $request->address;
            $userinfo->phone = $request->phone;
            //$userinfo->postcode = $request->postcode;
            $userinfo->save();

            
            return response()->json(['status'=>'success']);


        }
        
        
//        send email confirmation
        
    }
    public function getUserInfo(Request $request){
        $user = $this->jwt->parseToken()->authenticate();
        $userId = $user->id;
        //return $userId;
        $userdata = DB::table('users')->where('users.id', '=', $userId)
                ->join('user_info', function ($userId) {
            $userId->on('users.id', '=', 'user_info.userId');
        })->select('users.id as id','users.username as username','users.email as email','user_info.firstname','user_info.lastname','user_info.address as address','user_info.phone')
        ->first();
        $videos = DB::table('video_purchased')->where('video_purchased.userId','=',$userId)->join('videos', function($userId){
            $userId->on('video_purchased.videoId','=','videos.id');
        })->select('videos.title')->get();

        //$userdata['videos'] = $videos;
        //return $videos;
        return response()->json(['results'=>$userdata]);
    }
    public function postBuyVideo(Request $request){
        $user = $this->jwt->parseToken()->authenticate();
        $userId = $user->id;
        $data = new VideoPurchase;
        $data->userId = $userId;
        $data->videoId = $request->videoId;
        $data->save();
        return $userId;
    }
    public function postSearchQuery(Request $request){
        $queryJustkeywords = DB::table('videos')->where('title','like',$request->keywords.'%')->get();
        return $queryJustkeywords;
        return $request->all();
    }
}
