<?php

namespace App\Http\Controllers;

use App\User;
use App\UserInfo;
use App\Category;
use App\Label;
use App\Video;
use App\VideoPrices;
use App\VideoCat;
use App\VideoLab;
use App\VideoBundling;
use App\VideoInBundling;
use App\s3;
use DB;
use Illuminate\Http\Request;
use Illuminate\Hashing\BcryptHasher;
use FFMpeg;
use Tymon\JWTAuth\JWTAuth;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $jwt;

    public function __construct(JWTAuth $jwt)
    {
        $this->jwt = $jwt;
    }


    public function postLogin(Request $request)
    {
        return $request->all();
    }
    public function getDashboard(){
        $totvideos = Video::count();
        $totusers = User::where('role_id',1)->count();
        $totcategory = Category::count();

        return response()->json(['results'=>['totVideo'=>$totvideos,'totUser'=>$totusers,'totCategory'=>$totcategory]]);
    }
    public function getVideos()
    {
//        $data = Video::all();

//        return response()->json([$data]);
        $data = DB::table('videos')
                ->leftJoin('video_prices','videos.id','=','video_prices.videoId')
                ->select('videos.id as id','title', 'uid','description','thumbnail','poster','videos.duration','video_prices.price','videos.viewCount','videos.rating','videos.url')
                ->orderBy('created_at', 'desc')->get();
        $i = 0;
        $c = [];
        $l = [];
        foreach ($data as $d) {
            
            $ada = VideoCat::where('videoId',$d->id)->leftJoin('video_categorization','video_categories.catId','=','video_categorization.id')->select('video_categories.catId as id','video_categorization.name as name')->get();
            $adaLab = VideoLab::where('videoId',$d->id)->get();

            if ($ada) {
                # code...
                foreach ($ada as $a) {
                    $c[] = $a;

                }
                $cat = $c;
            } else {
                # code...
                $cat = [];
            }

            if ($adaLab) {
                # code...
                foreach ($adaLab as $al) {
                    $l[] = $al->labelId;
                }
                $lab = $l;
            } else {
                # code...
                $lab = [];
            }
            $d->category = $cat;
            $d->label = $lab;
            $baru[] = $d;
            $c = [];
            $cat = [];
            $l = [];
            $lab = [];
//            $baru[]['cat'] = $cat;
//            $data['cat'] = $cat;
              
        }
//        return response()->json(['results'=>$cat]);
//        $data->cat = $catVid;
        //return $data;
        return response()->json(['results'=>$baru]);
    }
    public function deleteVideoId($id){
        //delete video id
        $video = Video::find($id);
        $video->delete();
        //delete video category
        $video_cat = VideoCat::where('videoId',$id)->delete();
        //delete video label
        $video_lab = VideoLab::where('videoId',$id)->delete();
        //delete video price
        $video_price = VideoPrices::where('videoId',$id)->delete();
        return 'delete this video';
    }
    public function getVideoId($id)
    {
        $data = DB::table('videos')
                ->where('videos.id',$id)
                ->leftJoin('video_prices','videos.id','=','video_prices.videoId')
                ->select('videos.id as id','videos.uid','videos.title','videos.url','videos.description','videos.duration','videos.thumbnail','videos.poster','video_prices.price as price','videos.rating')
                ->first();
        //return $data;
        $category = DB::table('video_categories')->where('videoId',$id)->select('catId')->get();
        $cat_id = [];
        foreach ($category as $key) {
            //$cat_id[] = intval($key->catId);
            $cat_id[]= Category::find(intval($key->catId));
        }
        $data->category = $cat_id;
        $label = DB::table('video_labels')->where('videoId',$id)->select('labelId')->get();
        $lab_id = [];
        foreach ($label as $key) {
            $lab_id[] = Label::find(intval($key->labelId));
        }
        $data->label = $lab_id;
        return response()->json(['results'=>$data]);
    }
    public function uploadVideo(Request $request){
        $destinationPath = 'videos';
        $extension = $request->file('video')->getClientOriginalExtension();
        $filename = str_random(40);
        $request->file('video')->move($destinationPath,$filename.'.'.$extension);
        $videoUrl = url('/videos').'/'.$filename.'.'.$extension;
        return response()->json(['status'=>'success','videoUrl'=>$videoUrl]);
    }
    public function postVideoUpload(Request $request){
        //return $request->all();
        $destinationPath = 'videos';
        $extension = $request->file('video')->getClientOriginalExtension();
        $filename = $request->filename;
        $request->file('video')->move($destinationPath,$filename.'.'.$extension);
        $videoUrl = url('/videos').'/'.$filename.'.'.$extension;

        return response()->json(['results'=>['status'=>'success','url'=>$videoUrl]]);
    }
    public function postVideoReUpload(Request $request){
        //return $request->all();
        $destinationPath = 'videos';
        $extension = $request->file('video')->getClientOriginalExtension();
        $filename = $request->filename;
        $request->file('video')->move($destinationPath,$filename.'.'.$extension);
        $videoUrl = url('/videos').'/'.$filename.'.'.$extension;
                Video::where('id', $request->id)
          ->update(['url' => $videoUrl]);
        return response()->json(['results'=>['status'=>'success','url'=>$videoUrl]]);
    }

    public function postBundlingVideo(Request $request){
        //return $request->all();
        //save to table video_bundling
        $data = new VideoBundling;
        $data->name = $request->title;
        $data->description = $request->description;
        $data->poster = $request->posterUrl;
        $data->price = $request->price;
        $data->rating = $request->rating;
        $data->save();

        if ($request->has('video')) {
            foreach ($request->video as $video) {
                $vid = new VideoInBundling;
                $vid->video_bundling_id = $data->id;
                $vid->video_id = $video['id'];
                $vid->save();

            }
        } else {
            # code...
        }
        
        return 'berhasil di save';
    }
    public function getVideoBundling(Request $request){
        $data = VideoBundling::all();
        return response()->json(['results'=>$data]);
    }
    public function getVideoBundlingId($id,Request $request){
        $data = VideoBundling::find($id);
        $data_in_bundling = VideoInBundling::where('video_bundling_id',$id)
        ->leftJoin('videos','videos.id','=','video_in_bundling.video_id')->get();
        foreach ($data_in_bundling as $inbundling) {
            # code...
        }
        $data['video'] = $data_in_bundling;
        return response()->json(['results'=>$data]);
    }

    public function postVideo(Request $request)
    {

        //return $request->all();
        //$destinationPath = 'videos';
        //$extension = $request->file('video')->getClientOriginalExtension();
        //$filename = str_random(40);
      //  $request->file('video')->move($destinationPath,$filename.'.'.$extension);

        //$ffmpeg = FFMpeg\FFMpeg::create();


        $video = new Video;
        $video->title = $request->filename;
        $video->description = $request->description;
        $video->url = $request->videoUrl;
        //$video->url = url('/videos/53e9f8b8-1171-413e-8f95-f8b943e51e7e.mp4');
        $video->uid=$request->filename;
        //$video->price = $request->price;
        $video->duration = $request->duration;
        //$video->duration = 239.05;
        $video->thumbnail = '';
        $video->poster = $request->posterUrl;
        $video->save();
        $id = $video->id;
        $ada = VideoPrices::where('videoId',$id)->first();
        //return $ada;
        if ($ada) {
            $ada->price = $request->price;
            $ada->save();
        } else {
            $price = new VideoPrices;
            $price->videoId = $id;
            $price->price = $request->price;
            $price->save();
        };

        if ($request->has('category')) {
            $cat = $request->category;
            //return $cat;
            $adaCat = VideoCat::where('videoId',$id)->get();
//        return $adaCat;
            if (!$adaCat->isEmpty()) {
                # code...
                $adaCat = VideoCat::where('videoId',$id)->delete();
                foreach ($cat as $i) {
            # code...
                    $data = new VideoCat;
                    $data->videoId = $id;
                    $data->catId = $i;
                    $data->save();
                }
            } else {
                # code...
                foreach ($cat as $i) {
                # code...
                    $data = new VideoCat;
                    $data->videoId = $id;
                    $data->catId = $i;
                    $data->save();
                }
            }
        } else {
            
        }

        if ($request->has('label')) {
            $lab = $request->label;
            $adaLab = VideoLab::where('videoId',$id)->get();
            if (!$adaLab->isEmpty()) {
                # code...
                $adaLab = VideoLab::where('videoId',$id)->delete();
                foreach ($lab as $i) {
            # code...
                    $data = new VideoLab;
                    $data->videoId = $id;
                    $data->labelId = $i;
                    $data->save();
                }
            } else {
                # code...
                foreach ($lab as $i) {
                # code...
                    $data = new VideoLab;
                    $data->videoId = $id;
                    $data->labelId = $i;
                    $data->save();
                }
            }
        } else {
            
        }
        return  $request->all();
    }

    public function updateVideoId(Request $request, $id){
        $data = Video::find($id);
        $data->title = $request->title;
        $data->description = $request->description;
        $data->rating = $request->rating;
        $data->save();

        $ada = VideoPrices::where('videoId',$id)->first();
        //return $ada;
        if ($ada) {
            $ada->price = $request->price;
            $ada->save();
        } else {
            $price = new VideoPrices;
            $price->videoId = $data->id;
            $price->price = $request->price;
            $price->save();
        };
        if ($request->has('category')) {
            $cat = $request->category;
            $adaCat = VideoCat::where('videoId',$id)->get();
//        return $adaCat;
        $adaCat = VideoCat::where('videoId',$id)->delete();
        //return $cat;
         foreach ($cat as $i) {
            # code...
                $data = new VideoCat;
                $data->videoId = $id;
                $data->catId = $i['id'];
                $data->save();
            }
        } else {
            
        }

        if ($request->has('label')) {
            $lab = $request->label;
            $adaLab = VideoLab::where('videoId',$id)->get();
        //return $lab;
        $adaLab = VideoLab::where('videoId',$id)->delete();
         foreach ($lab as $i) {
            # code...
                $data = new VideoLab;
                $data->videoId = $id;
                $data->labelId = $i['id'];
                $data->save();
            }
        } else {
            
        }
        
        
        
//        $cat = $request->cat['selected'];
        //return $cat;
        
        //return $adaCat;
        //delete semua category atau insert into database
      
        //end delete
       
        
        return response()->json(['status'=>'success']);
    }

    public function postCategory(Request $request){
        $category = new Category;
        $category->slug = str_slug($request->name,"-");
        $category->name = $request->name;
        $category->description = $request->description;
        $category->save();
        return response()->json(['status'=>'success']);
    }

    public function getCategory(){
        $data = Category::all();
        return response()->json(['results'=>$data]);
    }

    public function deleteCategoryId($id){
        $data = Category::find($id);
        $data->delete();
        return response()->json(['status'=>'success']);
    }
    public function updateCategoryId($id, Request $request){
        $data = Category::find($id);
        $data->name = $request->name;
        $data->slug = str_slug($request->name,"-");
        $data->description = $request->description;
        $data->save();
        return response()->json(['status'=>'success']);
    }

    public function getLabel(){
        $data = Label::all();
        return response()->json(['results'=>$data]);
    }

    public function postLabel(Request $request){
        $category = new Label;
        //$category->slug = str_slug($request->name,"-");
        $category->name = $request->name;
        $category->description = $request->description;
        $category->save();
        return response()->json(['status'=>'success']);
    }

    public function updateLabelId($id, Request $request){
        $data = Label::find($id);
        $data->name = $request->name;
//        $data->slug = str_slug($request->name,"-");
        $data->description = $request->description;
        $data->save();
        return response()->json(['status'=>'success']);
    }

    public function deleteLabelId($id){
        $data = Label::find($id);
        $data->delete();
        return response()->json(['status'=>'success']);
    }

    public function getUser(){
        $users = DB::table('users')
                ->where('role_id',1)
                ->leftJoin('user_info','users.id','=','user_info.userId')
                ->select('users.id as id','username', 'email','user_info.firstname','user_info.lastname','user_info.address','user_info.phone','user_info.postcode','user_info.profilePic')
                ->get();
        return response()->json(['results'=>$users]);
    }
    public function getUserId($id){
        $user = DB::table('users')->where('user_info.userId', '=', $id)
                ->join('user_info', function ($id) {
            $id->on('users.id', '=', 'user_info.userId');
        })->select('users.id as id','users.username as username','users.email as email','user_info.firstname','user_info.lastname','user_info.address as address','user_info.phone','user_info.profilePic')
        ->first();
        $videos = DB::table('video_purchased')->where('video_purchased.userId','=',$id)->join('videos', function($userId){
            $userId->on('video_purchased.videoId','=','videos.id');
        })->select('videos.id','videos.title')->get();
        //var_dump($user_info);
        $user = (array)$user;
//        $user['firstname'] = $user_info->firstname;
//        $user['lastname'] = $user_info->lastname;
//        $user['address'] = $user_info->address;
        $user['videos'] = $videos;
        $user = (object)$user;
        //$user->firstname = $user_info['firstname'];
        return response()->json(['results'=>$user]);
    }

    public function postUserInfo(Request $request,$id){
        //return $id;

        //cek dulu apakah user id tersebut sudah ada infonya

        //if exist

        //replace info

        //else

        //create new
        $userinfo = new UserInfo;
        $userinfo->userId = $id;
        $userinfo->firstname = $request->firstname;
        $userinfo->lastname = $request->lastname;
        $userinfo->address = $request->address;
        $userinfo->phone = $request->phone;
        $userinfo->postcode = $request->postcode;
        $userinfo->save();

        return response()->json(['status'=>'success']);
    }

    public function postRegister(Request $request)
    {  
        $user = new User;
        $user->username = $request->email;
        $user->email = $request->email;
        $user->password = (new BcryptHasher)->make($request->password);
        $user->role_id = $request->role_id;
        $user->save();

//        send email confirmation
        return response()->json(['status'=>'success']);
    }

    public function updateThumb(Request $request){
       //return $request->all();



        $destinationPath = 'images';
        $extension = $request->file('file')->getClientOriginalExtension();
        $filename = str_random(12);
        $request->file('file')->move($destinationPath,$filename.'.'.$extension);
        
        $data = Video::find($request->id);
        $data->thumbnail = url('/images').'/'.$filename.'.'.$extension;
        $data->save();
        return $request->all();
    }
    public function updatePoster(Request $request){
       //return $request->all();



        $destinationPath = 'images';
        $extension = $request->file('file')->getClientOriginalExtension();
        $filename = $request->thumbfilename;
        $request->file('file')->move($destinationPath,$filename.'.'.$extension);
        
        $data = Video::find($request->id);
        $data->poster = url('/images').'/'.$filename.'.'.$extension;
        $data->save();
        return $request->all();
    }

    public function uploadPoster(Request $request){
        $destinationPath = 'images';
        $extension = $request->file('file')->getClientOriginalExtension();
        $filename = $request->thumbfilename;
        $request->file('file')->move($destinationPath,$filename.'.'.$extension);
        $data['status'] = 'success';
        $data['posterUrl'] = '/'.$destinationPath.'/'.$filename.'.'.$extension;
        return response()->json(['results'=>$data]);
    }

    public function getProfile(){
        $user = $this->jwt->parseToken()->authenticate();

        return response()->json(['results'=>$user]);

    }

    public function uploadS3(Request $request){
        //return $request->all();
        S3::putBucket('hehe');
        return 'hehe';
        $destinationPath = 'images';
        $extension = $request->file('file')->getClientOriginalExtension();
        $filename = $request->thumbfilename.'.'.$extension;
        //$request->file('file')->move($destinationPath,$filename.'.'.$extension);
        $data['status'] = 'success';
        $data['posterUrl'] = '/'.$destinationPath.'/'.$filename.'.'.$extension;
        
        $file = $request->file('file');
        $bucket_name = 'artistpress';
        $upload_name = 'aaa.jpg';
        $KilatStorageAccessKey = '008df51639a340f117d3';
        $KilatStorageSecretKey = 'GHTzB6/05myWTHv+Ps6ebPCFBUwPPB/DadBbIIHT';
        $s3 = new S3($KilatStorageAccessKey, $KilatStorageSecretKey);
        S3::putObject(S3::inputFile($file, true), $bucket_name, $filename, S3::ACL_PUBLIC_READ);
        return response()->json(['results'=>$data]);
    }
    public function getImportantVideo(){
        $featured = Video::orderBy('created_at','desc')->select('videos.id as id','title','url','poster','rating','duration','viewCount')->first();
        $price = VideoPrices::where('videoId',$featured->id)->select('price')->first();
        $featured['price'] = $price->price;
//        return $featured;
        $labelName1 = Label::find(1);
        $labelName2 = Label::find(2);
        $label1 = VideoLab::where('labelId',1)
                ->leftJoin('video_labeling','video_labeling.id','=','video_labels.labelId')
                ->leftJoin('videos','videos.id','=','video_labels.videoId')
                ->leftJoin('video_prices','video_prices.id','=','video_labels.videoId')
                ->select('videos.id as id','title','url','poster','rating','duration','viewCount','price')
                ->orderBy('videos.rating','desc')
                ->take(5)->get();
        $label2 = VideoLab::where('labelId',2)
                ->leftJoin('video_labeling','video_labeling.id','=','video_labels.labelId')
                ->leftJoin('videos','videos.id','=','video_labels.videoId')
                ->leftJoin('video_prices','video_prices.id','=','video_labels.videoId')
                ->select('videos.id as id','title','url','poster','rating','duration','viewCount','price')
                ->orderBy('videos.rating','desc')
                ->take(5)->get();
        $bundling = VideoBundling::take(5)->get();
        foreach ($bundling as $bund) {
            $bund->video = VideoInBundling::where('video_bundling_id',$bund->id)->leftJoin('videos','videos.id','=','video_in_bundling.video_id')->select('video_in_bundling.video_id as id','videos.title as title','videos.poster as poster','videos.rating as rating','videos.duration as duration','videos.viewCount as viewCount')->get();
            $bund_result[] = $bund;
        }
        return response()->json(['results'=>['featured'=>$featured,'label1'=>['name'=>$labelName1->name,'videos'=>$label1],'label2'=>['name'=>$labelName2->name,'videos'=>$label2],'bundling'=>$bund_result]]);
        //$video1 = Video::;
    }

}
