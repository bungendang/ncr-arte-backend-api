<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});


    $app->group(['prefix' => 'v1','namespace'=>'App\Http\Controllers','middleware' => ['cors','jwt.auth']],function($app){
        $app->get('/myinfo','UserController@getUserInfo');
        $app->post('/buy/video','UserController@postBuyVideo'); // beli video
        $app->get('admin/profile','AdminController@getProfile'); //ambil profile
        $app->post('/search/','UserController@postSearchQuery');

    });

$app->group(['prefix' => 'v1','namespace'=>'App\Http\Controllers','middleware'=>'cors'], function () use ($app) {
    //$app->post('admin/auth', 'AdminController@postLogin');



    $app->get('video/important','AdminController@getImportantVideo');
    

    $app->post('/auth/login', 'Auth\AuthController@postLogin'); //login user
    $app->post('/auth/admin', 'Auth\AuthController@postLogin'); //login user

    $app->get('dashboard','AdminController@getDashboard');

    $app->post('video','AdminController@postVideo'); //upload new video

    $app->post('category','AdminController@postCategory');
    $app->delete('category/{id}','AdminController@deleteCategoryId');
    $app->put('category/{id}','AdminController@updateCategoryId');
    
    $app->post('bundling','AdminController@postBundlingVideo'); //upload new video

    $app->post('label','AdminController@postLabel');
    $app->delete('label/{id}','AdminController@deleteLabelId');
    $app->put('label/{id}','AdminController@updateLabelId');

    $app->post('upload/thumb','AdminController@updateThumb');
    $app->post('update/poster','AdminController@updatePoster');
    $app->post('upload/poster','AdminController@uploadPoster');
    $app->post('upload/video','AdminController@postVideoUpload'); //upload new video
    $app->post('reupload/video','AdminController@postVideoReUpload'); //upload 

	$app->get('videos','AdminController@getVideos');
    $app->get('videos/{id}','AdminController@getVideoId');
    $app->put('videos/{id}','AdminController@updateVideoId');
    $app->delete('videos/{id}','AdminController@deleteVideoId');
	$app->get('users','AdminController@getUser');
    $app->get('users/{id}','AdminController@getUserId');
	$app->get('category','AdminController@getCategory');
	$app->get('label','AdminController@getLabel');
    $app->get('bundling','AdminController@getVideoBundling');
    $app->get('bundling/{id}','AdminController@getVideoBundlingId');





	$app->post('info/{id}','AdminController@postUserInfo');
    $app->post('user/register','UserController@postRegister');
    $app->post('admin/register','AdminController@postRegister');

    $app->post('test/upload','ExampleController@test');

    $app->post('/search/','UserController@postSearchQuery');

});