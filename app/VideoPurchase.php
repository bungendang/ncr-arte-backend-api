<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoPurchase extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'video_purchased';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */


    public $timestamps = false;
}
