<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoCat extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'video_categories';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    //  'created_at','updated_at'  
    ];

    public $timestamps = false;
}
