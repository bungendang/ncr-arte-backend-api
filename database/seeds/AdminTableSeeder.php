<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Hashing\BcryptHasher;


class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('user')->insert([
          //  'username' => 'admin',
            //'email' => 'webmaster@artestudio.co.id',
            //'password' => (new BcryptHasher)->make('janganlupa'),
        //]);
        $faker = Faker\Factory::create();
        foreach (range(1,10) as $index) {
            DB::table('users')->insert([
                'username' => $faker->userName,
                'email' => $faker->email,
                'password' => (new BcryptHasher)->make('janganlupa'),
                'role_id'=>1,
            ]);
            DB::getPdo()->lastInsertId();
            DB::table('user_info')->insert([
                'userId'=>DB::getPdo()->lastInsertId(),
                'firstname'=>$faker->firstName,
                'lastname'=>$faker->lastName,
                'address'=>$faker->address,
                'phone'=>$faker->phoneNumber,
                'profilePic'=>$faker->imageUrl($width = 480, $height = 480,'people'),
                'postcode'=>$faker->postcode,
            ]);
        }
    }
}