<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideoPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('videoId');
            $table->float('price');
            $table->float('discount');
            $table->decimal('discountPercent',3,2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('video_prices');
    }
}
