<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_info', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userId');
            $table->string('firstname',20);
            $table->string('lastname',20);
            $table->text('address');
            $table->integer('cityId');
            $table->integer('provinceId');
            $table->integer('countryId');
            $table->integer('postcode');
            $table->string('phone',14);
            $table->string('profilePic');
            $table->string('imei',25);
            $table->string('imsi',25);
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_info');
    }
}
