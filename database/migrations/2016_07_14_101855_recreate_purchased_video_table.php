<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RecreatePurchasedVideoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_purchased', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('videoId');
            $table->integer('userId');
            $table->dateTime('purchasedDate');
            $table->dateTime('expiredDate');
            $table->string('invoiceNum');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('video_purchased');
    }
}
